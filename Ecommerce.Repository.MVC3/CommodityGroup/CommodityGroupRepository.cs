﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DBReader;
using Ecommerce.Extensions;

namespace Ecommerce.Repository.CommodityGroup
{
	public class CommodityGroupRepository : ICommodityGroupRepository
	{
		public string GetCommodityGroupContentSql(Guid commodityGroup)
		{

			var ds = DBHelper.GetDataSetMTSql(@"

SELECT 
	*
FROM
	t_CommodityGroup
WHERE
	OID = @OID

SELECT
	*
FROM
	t_CommodityGoods
WHERE
	OID = @OID

SELECT 
	*
FROM
	t_CommodityManufThemes
WHERE
	OID = @OID

", new Hashtable { { "OID", commodityGroup } }, false, null);
			var goods = ds.Tables[1].AsEnumerable().Select(row => row.Field<Guid>("goodsOID")).ToList();
			var manufThemes = ds.Tables[2].AsEnumerable().Select(row =>
				new
				{
					ManufacturerOID = row.Field<Guid?>("manufacturerOID") ?? Guid.Empty,
					ThemeOID = row.Field<Guid?>("themeOID") ?? Guid.Empty
				}).ToList();
			var sb = new StringBuilder();
			sb.AppendFormat(@"   
				t_Goods g
				left join t_Company c on g.manufacturer = c.OID
				inner join t_GoodsInStocks gs on g.OID = gs.OID And gs.shopType = @shopType 
				inner join t_GoodsPrices gpr on g.OID = gpr.OID And gpr.shopType = @shopType
				inner join t_Object o on g.OID = o.OID and 
					(");
		    if (manufThemes.Count == 0 && goods.Count == 0)
		    {
		        sb.Append(" 1 <> 1 ");
		    }
			else if (manufThemes.Count != 0)
			{
				manufThemes
					.Where(item => item.ManufacturerOID != Guid.Empty)
					.Select(item => item.ManufacturerOID)
					.Distinct()
					.ToList()
					.ForEach(manuf =>
					{
						if (manufThemes
							.Count(item => item.ManufacturerOID == manuf && item.ThemeOID == Guid.Empty) > 0)
						{
							sb.AppendFormat(@"
					(	c.OID = '{0}'
					) or ", manuf);
						}
						else
						{
							var themes = manufThemes
								.Where(item => item.ManufacturerOID == manuf && item.ThemeOID != Guid.Empty)
								.Select(item => item.ThemeOID)
								.Distinct()
								.ToList();
							sb.Append(@"  
					(
						g.category in 
						(
							SELECT tm.masterOID
							FROM t_Nodes n
								inner join t_Nodes n2 on n.tree = n2.tree and n.lft BETWEEN n2.lft and n2.rgt
								inner join t_Nodes n1 on n2.tree = n1.tree And n2.lft > n1.lft And n2.lft < n1.rgt And n1.nodeID = @rootNode
								inner join t_Theme t on t.OID = n2.object");
							sb.Append(" and t.OID");
							sb.Append(themes.InClause());
							sb.AppendFormat(@"
								inner join t_ThemeMasters tm on n.object = tm.OID
						)
						and c.OID = '{0}'
					) or ", manuf);
						}
					});
				var pureThemes = manufThemes
					.Where(item => item.ManufacturerOID == Guid.Empty)
					.Select(item => item.ThemeOID)
					.Distinct()
					.ToList();
				if (pureThemes.Count > 0)
				{
					sb.Append(@"  
					(
						g.category in 
						(
							SELECT tm.masterOID
							FROM t_Nodes n
								inner join t_Nodes n2 on n.tree = n2.tree and n.lft BETWEEN n2.lft and n2.rgt
								inner join t_Nodes n1 on n2.tree = n1.tree And n2.lft > n1.lft And n2.lft < n1.rgt And n1.nodeID = @rootNode
								inner join t_Theme t on t.OID = n2.object");
					sb.Append(" and t.OID");
					sb.Append(pureThemes.InClause());
					sb.Append(@"
								inner join t_ThemeMasters tm on n.object = tm.OID
						)
					) or ");
				}

				sb.Length -= 3;
			}

            if (goods.Count != 0)
			{
			    var oper = ""; // "and";
				if (manufThemes.Count != 0) oper = "or";
				sb.AppendFormat(@"
					{0} o.OID", oper);
				sb.Append(goods.InClause());
			}
			sb.Append(@"
				)
");
			return sb.ToString();
		}

		public void GenerateCommodityGroupContent(Guid commodityGroup, bool hasCompanyName, bool hasModelName, bool hasGoodsParams)
		{
			const string sql = @" 
Declare
	@shopType int,
	@rootNode int

SELECT @shopType = shopType FROM t_CommodityGroup WHERE OID = @OID
SELECT @rootNode = themeRootNode FROM t_TypeShop WHERE shopType = @shopType

DELETE FROM t_CommodityActualGoods WHERE OID = @OID

INSERT INTO t_CommodityActualGoods (OID, goodsOID)
SELECT @OID, g.OID FROM
{0}
WHERE
	{1} and {2} and gpr.price > 0 and {3}

UPDATE t_CommodityActualGoods SET ordValue = cg.ordValue
FROM t_CommodityActualGoods cag
	inner join t_CommodityGoods cg on cag.OID = cg.OID and cg.goodsOID = cag.goodsOID and cg.OID = @OID

UPDATE t_CommodityActualGoods SET ordValue = 100000
WHERE OID = @OID and ordValue is null

";
		    var sqlExec = string.Format(sql,
		                                GetCommodityGroupContentSql(commodityGroup),
		                                hasCompanyName ? "ISNULL(c.companyName, '') <> ''" : "1=1",
		                                hasModelName ? "ISNULL(g.model, '') <> ''" : "1=1",
		                                hasGoodsParams ? "EXISTS (SELECT * FROM t_GoodsParams WHERE OID = g.OID)" : "1=1");
			DBHelper.ExecuteCommand(sqlExec, new Hashtable { { "OID", commodityGroup } }, false);
		}

		public int GetCommodityGroupShop(Guid commodityGroup)
		{
			return "SELECT shopType FROM t_CommodityGroup WHERE OID = @commodityGroup".ExecSql(new { commodityGroup })
				.AsEnumerable()
				.Select(row => row.Field<int>("shopType"))
				.Single();
		}
	}
}
