using System;

namespace Ecommerce.Repository.CommodityGroup
{
	public interface ICommodityGroupRepository
	{
		string GetCommodityGroupContentSql(Guid commodityGroup);

		void GenerateCommodityGroupContent(Guid commodityGroup, bool hasCompanyName, bool hasModelName, bool hasGoodsParams);

		int GetCommodityGroupShop(Guid commodityGroup);
	}
}