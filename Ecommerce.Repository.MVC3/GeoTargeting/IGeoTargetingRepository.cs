using System;
using System.Collections.Generic;

namespace Ecommerce.Repository.GeoTargeting
{
    public interface IGeoTargetingRepository
    {
        long AddressToInt(string ip);

        int GetRegion(string ip);

        string GetRegionName(int regionCode);

        int GetRegionShop(int regionCode, int dafaultShop);

        IList<Guid> GetParentLocation(int regionCode);

        bool IsChild(Guid RegionID, int regionCode);

        string GetInetAddr(int regionCode);
    }
}