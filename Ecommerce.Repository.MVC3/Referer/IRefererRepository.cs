﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecommerce.Repository.Referer
{
	public interface IRefererRepository
	{
		string GetLabel(string host, string referQuery, string urlQuery);
		string GetName(int id);
	}
}
