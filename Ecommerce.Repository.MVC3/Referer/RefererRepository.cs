﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Core.Aspects;
using DBReader;
using System.Data;
using Ecommerce.Extensions;
using NLog;

namespace Ecommerce.Repository.Referer
{
	public class RefererRepository : IRefererRepository
	{
		private Logger log = LogManager.GetCurrentClassLogger();
		private static readonly Regex openStatReg = new Regex("_openstat=([^&]*)", RegexOptions.IgnoreCase);
		private static readonly Regex utmSourceReg = new Regex("utm_source=([^&]*)", RegexOptions.IgnoreCase);
		private static readonly Regex engText = new Regex(@"[^0-9A-Za-z;: \.]", RegexOptions.IgnoreCase);
		private IRefererLabelCalc _labelCalc;
		public RefererRepository(IRefererLabelCalc calc)
		{
			_labelCalc = calc;
		}
		public string GetLabel(string host, string referQuery, string urlQuery)
		{ 
			host = GetHost(host, referQuery, urlQuery);
			const string sql = @"
begin tran

if NOT EXISTS(SELECT * FROM t_Referer WHERE name = @host) Begin
	INSERT INTO t_Referer(name) VALUES(@host)
	SELECT id, code, isNew = convert(bit, 1) FROM t_Referer WHERE id = @@IDENTITY 
End 
else Begin
	SELECT id, code, isNew = convert(bit, 0) FROM t_Referer WHERE name = @host 
end

commit tran	
";
			const string sqlUpdate = @"
UPDATE t_Referer SET code = @code WHERE id = @id 
";
			var codeRec = sql.ExecSql(new { host }).Select(row => new { id = row.Field<int>("id"), code = row.Field<string>("code"), isNew = row.Field<bool>("isNew") }).Single();
			if (codeRec.isNew)
			{
				var code = _labelCalc.Calculate(codeRec.id);
				DBHelper.ExecuteCommand(sqlUpdate, new Hashtable {{"id", codeRec.id}, {"code", code}}, false);
				return code;
			}
			return codeRec.code;
		}
		private string GetHost(string host, string referQuery, string urlQuery)
		{
			var utmHost = GetUtmSourceParam(urlQuery);
			if (!string.IsNullOrWhiteSpace(utmHost)) return utmHost;
			var openStat = GetOpenStatParam(referQuery);
			var urlOpenStat = GetOpenStatParam(urlQuery);
			string query = null;
			if (!string.IsNullOrEmpty(openStat))
			{
				query = DecodeOpenStat(openStat);
			}
			else if (!string.IsNullOrEmpty(urlOpenStat))
			{
				query = DecodeOpenStat(urlOpenStat);
			}

            if(!string.IsNullOrEmpty(query)) query = ModifyIfYandex(query);
			return query ?? host;
		}

	    private static string ModifyIfYandex(string url)
	    {
            if (url.IndexOf("direct.yandex.ru;", StringComparison.InvariantCultureIgnoreCase) != -1)
                return "direct.yandex.ru";
            if (url.IndexOf("market.yandex.ru;", StringComparison.InvariantCultureIgnoreCase) != -1)
                return "market.yandex.ru";
	        return url;
	    }

		private static string DecodeOpenStat(string openStat)
		{
			for (int i = 0; i < openStat.Length % 4; i++) openStat += '=';
			try
			{
				var rawData = Convert.FromBase64String(openStat);
				var sb = new StringBuilder();
				foreach (var b in rawData)
				{
					sb.Append((char)b);
				}
				return engText.Replace(sb.ToString(), "").TrimInner();
			}
			catch
			{
				return null;
			}
		}

		private static string GetUtmSourceParam(string queryString)
		{
			if (!string.IsNullOrEmpty(queryString))
			{
				var m = utmSourceReg.Match(queryString);
				return m.Success ? m.Groups[1].Value : null;
			}
			return null;
		}

		private string GetOpenStatParam(string queryString)
		{
			if (!string.IsNullOrEmpty(queryString))
			{
				var m = openStatReg.Match(queryString);
			    if (m.Success)
			    {
			        var openStat = m.Groups[1].Value;
			        var openStatSeg = openStat.Split(";".ToCharArray());
			        return openStatSeg.Length > 0 ? openStatSeg[0].Trim() : openStat;
			    }
			}
			return null;
		}

		[Cache]
		public string GetName(int id)
		{
			const string sql = "SELECT name FROM t_Referer WHERE id = @id";
			return sql.ExecSql(new {id}).Select(row => row.Field<string>("name")).SingleOrDefault();
		}
	}
}
