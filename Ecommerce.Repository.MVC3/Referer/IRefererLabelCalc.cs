﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecommerce.Repository.Referer
{
	public interface IRefererLabelCalc
	{
		string Calculate(int id);
		int ReverseCalculate(string id);
	}
}
