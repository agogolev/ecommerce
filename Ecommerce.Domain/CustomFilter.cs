﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecommerce.Domain
{
	public class CustomFilter : IEquatable<CustomFilter>
	{
		public Guid ParamType { get; set; }
		public string Name { get; set; }
		public int ItemsCount { get { return Values.Sum(v => v.ItemsCount); } }
		public IList<CustomFilterValue> Values { get; set; }

		#region IEquatable<PriceInterval> Members

		public bool Equals(CustomFilter other)
		{
			// Check whether the compared object is null.
			if (Object.ReferenceEquals(other, null)) return false;

			// Check whether the compared object references the same data.
			if (Object.ReferenceEquals(this, other)) return true;

			return this.ParamType.Equals(other.ParamType);
		}

		#endregion
		public override int GetHashCode()
		{
			return ParamType.GetHashCode();
		}
		public bool HasGoodsParam(ParamItem item)
		{
			var hasParam = item.ParamType.Equals(this.ParamType) &&
				Values
					.SelectMany(k => k.Associations)
					.Count(k => string.Compare(item.ParamValue, k.Value, true) == 0 && string.Compare(item.Unit, k.Unit, true) == 0) != 0;
			return hasParam;
		}
		public bool HasGoods(GoodsItemBase goods)
		{
			return goods.GoodsParams.Any(k => HasGoodsParam(k));
		}
	}

	public static class CustomFilterExtension
	{
		public static bool NotEmpty(this IEnumerable<CustomFilter> items)
		{
			return items.Sum(item => item.Values.Sum(v => v.ItemsCount)) > 0;
		}

		public static bool NotEmpty(this CustomFilter item)
		{
			return item.Values.Sum(v => v.ItemsCount) > 0;
		}
	}

}
