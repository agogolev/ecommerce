using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ecommerce.Extensions;

namespace Ecommerce.Domain
{
	public class ParamItem : IEquatable<ParamItem>
	{
		public Guid ParamType { get; set; }
		public int TypeParam { get; set; }
		public string ParamName { get; set; }
		public string ParamValue { get; set; }
		public string Unit { get; set; }
		public bool IsFullName { get; set; }
		public bool IsFilter { get; set; }
		public HtmlString ValueAUnit
		{
			get { return string.Format("{0} {1}", ParamValue, Unit).ToHtml(); }
		}

		#region IEquatable<ParamItem> Members

		public bool Equals(ParamItem other)
		{
			// Check whether the compared object is null.
			if (ReferenceEquals(other, null)) return false;

			// Check whether the compared object references the same data.
			if (ReferenceEquals(this, other)) return true;

			return ParamType.Equals(other.ParamType);
		}

		#endregion

		public override int GetHashCode()
		{
			return ParamType.GetHashCode();
		}
		public ParamItem GetSameParam(GoodsItemBase item)
		{
			return item.GoodsParams.Where(k => k.ParamType == ParamType).DefaultIfEmpty(new ParamItem()).Select(k => k).Single();
		}
		public bool IsDifferent(IList<GoodsItemBase> items)
		{
			return !items.Select(k => k.GoodsParams).SelectMany(k => k).Where(k => k.ParamType == ParamType).All(k => string.Compare(k.ParamValue, ParamValue, true) == 0 && string.Compare(k.Unit, Unit, true) == 0);
		}
	}
}