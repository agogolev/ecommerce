﻿using System;

namespace Ecommerce.Domain
{
    public class PriceInterval : IEquatable<PriceInterval>
    {
        public decimal PriceBegin { get; set; }
        public decimal PriceEnd { get; set; }
        public bool Between(decimal price)
        {
            return price >= PriceBegin && price <= PriceEnd;
        }
        #region IEquatable<PriceInterval> Members

        public bool Equals(PriceInterval other)
        {
            // Check whether the compared object is null.
            if (ReferenceEquals(other, null)) return false;

            // Check whether the compared object references the same data.
            if (ReferenceEquals(this, other)) return true;

            return PriceBegin.Equals(other.PriceBegin) && PriceEnd.Equals(other.PriceEnd);
        }
        #endregion

        public override int GetHashCode()
        {
            var hashBegin = PriceBegin.GetHashCode();
            var hashEnd = PriceEnd.GetHashCode();

            // Calculate the hash code for the object.
            return hashBegin ^ hashEnd;
        }
    }
}
