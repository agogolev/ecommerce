namespace Ecommerce.Domain
{
	public class CustomFilterValueAssociation
	{
		public string Value { get; set; }
		public string Unit { get; set; }
	}
}