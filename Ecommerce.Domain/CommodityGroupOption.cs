﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecommerce.Domain
{
	public class CommodityGroupOption
	{
		public string Url { get; set; }
		public string PriceAggregator { get; set; }
		public string GlobalUtmLabel { get; set; }
		public bool IncludeBidCBid { get; set; }
		public bool YandexDtd { get; set; } 
		public bool ParamToDescription { get; set; }
		public bool TorgMailDtd { get; set; }
		public bool ForContext { get; set; }
        public bool ForABC { get; set; }
	}
}
