﻿using System.Collections.Generic;
using System.Linq;

namespace Ecommerce.Domain
{
    public static class PriceIntervalHelper
    {
        public static bool Between(this IList<PriceInterval> items, decimal price)
        {
            return items.Where(k => k.Between(price)).Count() != 0;
        }
    }
}
