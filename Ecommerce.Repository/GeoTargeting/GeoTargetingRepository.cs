﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Text.RegularExpressions;

namespace Ecommerce.Repository.GeoTargeting
{
	public class GeoTargetingRepository
	{
		private readonly DataTable dataTable;
		private readonly Regex ipRegex;
		
		public GeoTargetingRepository(string dbFileName)
		{
			var srMaster = new StreamReader(dbFileName, Encoding.GetEncoding(1251));

			dataTable = new DataTable("Master");
			dataTable.Columns.AddRange(new[]
			{   new DataColumn("Start", typeof(long)),
				new DataColumn("Stop", typeof(long)),
				new DataColumn("InetNum", typeof(string)),
				new DataColumn("Country", typeof(string)),
				new DataColumn("code", typeof(int))});


			string str;
			while ((str = srMaster.ReadLine()) != null)
			{
				var strs = str.Split('\t');

				var dataRow = dataTable.NewRow();
				dataRow["Start"] = long.Parse(strs[0]);
				dataRow["Stop"] = long.Parse(strs[1]);
				dataRow["InetNum"] = strs[2];
				dataRow["Country"] = strs[3];
				dataRow["code"] = strs[4] != "-" ? int.Parse(strs[4]) : 0;
				dataTable.Rows.Add(dataRow);
			}

			ipRegex = new Regex(@"^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$", RegexOptions.Compiled);
		}

		public long AddressToInt(string ip)
		{
			try
			{
				if (ipRegex.IsMatch(ip))
				{
					//проверяем корректность IP
					var strs = ip.Split('.');
					var ipNums = new int[4];
					for (var i = 0; i < ipNums.Length; i++)
						if ((ipNums[i] = int.Parse(strs[i])) < 0 || ipNums[i] > 255)
							return 0;

					return (long)ipNums[0] * 256 * 256 * 256 + (long)ipNums[1] * 256 * 256 + (long)ipNums[2] * 256 + ipNums[3];
				}
				return 0;
			}
			catch
			{
				return 0;
			}

		}
		public int GetRegion(string ip)
		{
			try
			{
				var ipNum = AddressToInt(ip);
				//Ищем позицию IP в основной сетке адресов делением шага пополам
				int min = 0, max = dataTable.Rows.Count;
				while (min < max)
				{
					var index = (min + max) / 2;

					if (ipNum > (long)dataTable.Rows[index]["Stop"])
					{
						min = index + 1; continue;
					}

					if (ipNum < (long)dataTable.Rows[index]["Start"])
					{
						max = index; continue;
					}

					if (ipNum >= (long)dataTable.Rows[index]["Start"] && ipNum <= (long)dataTable.Rows[index]["Stop"])
					{
						return (int)dataTable.Rows[index]["Code"];
					}
				}

				return 0;
			}
			catch
			{
				return 0;
			}
		}

		[Cache]
		public string GetRegionName(int regionCode)
		{
			const string sql = @"
SELECT
	name
FROM
	t_Location  
WHERE
	id = @regionCode
";
			return sql.ExecSql(new { regionCode }).AsEnumerable().Select(row => row.Field<string>("name")).DefaultIfEmpty("").Single();
		}

		[Cache]
		public int GetRegionShop(int regionCode, int dafaultShop)
		{
			const string sql = @"
SELECT TOP 1
	l.shopType
FROM
	t_Location l
	inner join t_Nodes n on l.OID = n.object and n.tree = '3094c215-57e7-4623-b4b9-6c93914e8c88'
	inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft and n.rgt 
	inner join t_Location l1 on n1.object = l1.OID  
WHERE
	l1.id = @regionCode and l.shopType is not null
ORDER BY n.lft DESC
";
            return sql.ExecSql(new { regionCode }).AsEnumerable().Select(row => row.Field<int>("shopType")).DefaultIfEmpty(dafaultShop).Single();
		}

		[Cache]
		public IList<Guid> GetParentLocation(int regionCode)
		{
			const string sql = @"
SELECT
	n.object
FROM
	t_Nodes n 
	inner join t_Nodes n1 on n.tree = n1.tree and n1.lft BETWEEN n.lft and n.rgt
	inner join t_Location l on n1.object = l.OID and l.id = @regionCode
ORDER BY
	n.lft
";
			return sql.ExecSql(new { regionCode })
				.AsEnumerable()
				.Where(row => row.Field<Guid?>("object").HasValue)
				.Select(row => row.Field<Guid>("object"))
				.ToList();
		}

		[Cache]
		public bool IsChild(Guid RegionID, int regionCode)
		{
			var IDs = GetParentLocation(regionCode);
			return IDs.Contains(RegionID);
		}

		public string GetInetAddr(int regionCode)
		{
			return dataTable.AsEnumerable().Where(row => row.Field<int>("code") == regionCode)
				.Select(row => row.Field<string>("InetNum")).DefaultIfEmpty("-").First();
		}
	}
}

