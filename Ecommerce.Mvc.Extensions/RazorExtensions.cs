﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.WebPages;
using System.Web.WebPages.Html;

namespace Ecommerce.Mvc.Extensions
{
    public static class RazorExtensions
    {
        // a quick and simple repeater helper method
        // header and footer templates could be added
        public static HelperResult Repeater<T>(
            this IEnumerable<T> items,
            Func<T, HelperResult> itemTemplate,
            Func<T, HelperResult> alternatingItemTemplate = null,
            Func<T, HelperResult> separatorTemplate = null)
        {
            return new HelperResult(writer =>
            {
                var enumerable = items as IList<T> ?? items.ToList();
                if (!enumerable.Any()) return;
                var last = enumerable.Last();
                int i = 0;

                foreach (var item in enumerable)
                {
                    var func = (i % 2 != 0 && alternatingItemTemplate != null) ? alternatingItemTemplate : itemTemplate;

                    func(item).WriteTo(writer);

                    if (separatorTemplate != null && !item.Equals(last))
                    {
                        separatorTemplate(item).WriteTo(writer);
                    }

                    i++;
                }
            });
        }
        public static IHtmlString Repeater<T>(
            this HtmlHelper html,
            IEnumerable<T> items,
            Func<T, HelperResult> itemTemplate,
            Func<T, HelperResult> alternatingItemTemplate = null,
            Func<T, HelperResult> separatorTemplate = null)
        {
            if (items == null)
                return new HtmlString("");

            var enumerable = items as IList<T> ?? items.ToList();
            if (!enumerable.Any()) return new HtmlString("");

            var last = enumerable.Last();
            var i = 0;
            var sb = new StringBuilder();
            foreach (var item in enumerable)
            {
                var func = (i % 2 != 0 && alternatingItemTemplate != null) ? alternatingItemTemplate : itemTemplate;
                sb.Append(func(item).ToHtmlString());
                if (separatorTemplate != null && !item.Equals(last))
                {
                    sb.Append(separatorTemplate(item).ToHtmlString());
                }
                i++;
            }

            return new HtmlString(sb.ToString());
        }
    }


}
