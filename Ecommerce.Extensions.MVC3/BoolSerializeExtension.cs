﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecommerce.Extensions
{
	public static class BoolSerializeExtension
	{
		public static string Int2BoolStr(this int item)
		{
			return item != 0 ? "true" : "false";
		}
	}
}
