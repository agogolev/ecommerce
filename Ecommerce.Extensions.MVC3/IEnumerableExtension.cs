﻿using System.Collections.Generic;

namespace Ecommerce.Extensions
{
	public static class IEnumerableExtension
	{
		public static IEnumerable<T> Intersperse<T>(this IEnumerable<T> source, T element)
		{
			bool first = true;
			foreach (T value in source)
			{
				if (!first) yield return element;
				yield return value;
				first = false;
			}
		}
		public static IEnumerable<T[]> GetSubarray<T>(this IEnumerable<T> items, int dimension)
		{
			var e = items.GetEnumerator();
			e.MoveNext();
			while (true)
			{
				if (e.Current == null) yield break;
				var ret = new T[dimension];
				for (var i = 0; i < dimension; i++)
				{
					ret[i] = e.Current;
					e.MoveNext();
				}
				yield return ret;
			}
		}
		public static IEnumerable<T[]> GetSubarray<T>(this IEnumerable<T> items, int dimension, T remainder)
		{
			var e = items.GetEnumerator();
			e.MoveNext();
			while (true)
			{
				if (e.Current == null) yield break;
				var ret = new T[dimension];
				for (var i = 0; i < dimension; i++)
				{
					ret[i] = e.Current != null ? e.Current : remainder;
					e.MoveNext();
				}
				yield return ret;
			}
		}
	}
}
