﻿using System.Xml.Linq;

namespace Ecommerce.Extensions
{
	public static class Linq2XmlExtension
	{
		public static string InnerXml(this XElement element)
		{
			if (element != null)
			{
				var reader = element.CreateReader();
				reader.MoveToContent();
				return reader.ReadInnerXml();
			}
			return string.Empty;
		}

		public static string OuterXml(this XElement element)
		{
			if (element != null)
			{
				var reader = element.CreateReader();
				reader.MoveToContent();
				return reader.ReadOuterXml();
			}
			return string.Empty;
		}
	}
}
