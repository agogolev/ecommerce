﻿using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;

namespace Ecommerce.Extensions
{
	public static class HtmlConvertExtension
	{
		private static Regex hasOpenTag = new Regex(@"\<[^\>]+\>");
		private static Regex hasCloseTag = new Regex(@"\</[^\>]+\>");
		public static string CrLf2Br(this string item)
		{
			return item == null ? null : item.Replace("\r", "").Replace("\n", "<br />");
		}

		public static HtmlString ToHtml(this string item)
		{
			if (string.IsNullOrEmpty(item)) return null;
			if(hasOpenTag.IsMatch(item) && hasCloseTag.IsMatch(item)) 
				return new HtmlString(item);
			return new HtmlString(item.Replace("\r", "").Replace("\n", "<br />"));
		}

		public static HtmlString DocToHtml(this string item)
		{
			if (string.IsNullOrEmpty(item)) return null;
            return new HtmlString(item);
		}

        public static string DocToHtmlString(this string item)
        {
            if (string.IsNullOrEmpty(item)) return null;
            var doc = XDocument.Parse(item);
            return doc.Root.InnerXml();
        }
	}
}
