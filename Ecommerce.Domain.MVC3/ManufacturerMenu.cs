﻿using System;

namespace Ecommerce.Domain
{
    public class ManufacturerMenu
    {
        public Guid OID { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortLink { get; set; }
        public int GoodsCount { get; set; }
        public string Url { get; set; }
    }
}
