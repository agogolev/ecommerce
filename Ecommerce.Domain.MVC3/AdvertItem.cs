﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecommerce.Domain
{
	public class AdvertItem : ICloneable
	{
		public Guid OID { get; set; }
		public int ObjectId { get; set; }
		public Guid? BinData { get; set; }
		public string Header { get; set; }
		public string Html { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public int AdvertType { get; set; }
		public string Url { get; set; }
		public Guid ShowKey { get; set; }
		public int ParentType { get; set; }
		public bool GeoInclusive { get; set; }
		public IList<int> GeoLocations { get; set; }
		public IList<Guid> Objects { get; set; }
		public int Weight { get; set; }

		#region ICloneable Members

		public object Clone()
		{
			return new AdvertItem
			{
				OID = this.OID,
				ObjectId = this.ObjectId,
				BinData = this.BinData,
				Header = this.Header,
				Html = this.Html,
				Width = this.Width,
				Height = this.Height,
				AdvertType = this.AdvertType,
				Url = this.Url,
				ShowKey = this.ShowKey,
				ParentType = this.ParentType,
				GeoInclusive = this.GeoInclusive,
				GeoLocations = this.GeoLocations,
				Objects = this.Objects,
				Weight = this.Weight
			};
		}

		#endregion
	}
}
