﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecommerce.Domain
{
	public class AdvertPlace : ICloneable
	{
		public Guid OID { get; set; }
		public int ObjectId { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public int ItemWidth { get; set; }
		public int ItemHeight { get; set; }
		public bool IsVerical { get; set; }
		public int ItemsCount { get; set; }
		public int AdvertPlaceType { get; set; }
		//public string MainCss { get; set; }
		//public string SliderCss { get; set; }
		public IList<AdvertItem> Items { get; set; }

		#region ICloneable Members

		public object Clone()
		{
			return new AdvertPlace { 
				OID = OID,
				ObjectId = ObjectId,
				Width = Width,
				Height = Height,
				ItemWidth = ItemWidth,
				ItemHeight = ItemHeight,
				IsVerical = IsVerical,
				ItemsCount = ItemsCount,
				AdvertPlaceType = AdvertPlaceType
			};
		}

		#endregion
	}
}
