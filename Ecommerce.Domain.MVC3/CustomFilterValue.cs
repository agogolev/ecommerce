using System;
using System.Collections.Generic;

namespace Ecommerce.Domain
{
	public class CustomFilterValue : IEquatable<CustomFilterValue>
	{
		public string Name { get; set; }
		public int OrdValue { get; set; }
		public int ItemsCount { get; set; }
		public IList<CustomFilterValueAssociation> Associations { get; set; }
		public Guid ParamType { get; set; } // ��� SelectMany
	    public int Id { get; set; }

		#region IEquatable<CustomFilter> Members

		public bool Equals(CustomFilterValue other)
		{
			// Check whether the compared object is null.
			if (Object.ReferenceEquals(other, null)) return false;

			// Check whether the compared object references the same data.
			if (Object.ReferenceEquals(this, other)) return true;

			return this.ParamType.Equals(other.ParamType) && this.OrdValue.Equals(other.OrdValue);
		}

		#endregion
		public override int GetHashCode()
		{
			int hashParam = ParamType.GetHashCode();
			int hashOrd = OrdValue.GetHashCode();

			return hashParam ^ hashOrd;
		}
	}
}