﻿using System.Collections.Generic;

namespace Ecommerce.Domain
{
	public class GoodsItemBase
	{
		public IList<ParamItem> GoodsParams { get; set; }
	}
}
