﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Ecommerce.Extensions
{
	public static class TranslitExtention
	{
		private static readonly Regex reSpaces = new Regex(@"\s+", RegexOptions.Singleline | RegexOptions.Compiled);
		private static readonly Regex reMinus = new Regex(@"-+", RegexOptions.Singleline);
		private static readonly Dictionary<char, string> translitEncodeTbl = new Dictionary<char, string> 
		{
			{'а', "a"},
			{'б', "b"},
			{'в', "v"},
			{'г', "g"},
			{'д', "d"},
			{'е', "e"},
			{'ё', "jo"},
			{'ж', "zh"},
			{'з', "z"},
			{'и', "i"},
			{'й', "jj"},
			{'к', "k"},
			{'л', "l"},
			{'м', "m"},
			{'н', "n"},
			{'о', "o"},
			{'п', "p"},
			{'р', "r"},
			{'с', "s"},
			{'т', "t"},
			{'у', "u"},
			{'ф', "f"},
			{'х', "kh"},
			{'ц', "c"},
			{'ч', "ch"},
			{'ш', "sh"},
			{'щ', "shh"},
			{'ъ', "iii"},
			{'ы', "y"},
			{'ь', "ii"},
			{'э', "eh"},
			{'ю', "ju"},
			{'я', "ja"},
			{'А', "A"},
			{'Б', "B"},
			{'В', "V"},
			{'Г', "G"},
			{'Д', "D"},
			{'Е', "E"},
			{'Ё', "JO"},
			{'Ж', "ZH"},
			{'З', "Z"},
			{'И', "I"},
			{'Й', "JJ"},
			{'К', "K"},
			{'Л', "L"},
			{'М', "M"},
			{'Н', "N"},
			{'О', "O"},
			{'П', "P"},
			{'Р', "R"},
			{'С', "S"},
			{'Т', "T"},
			{'У', "U"},
			{'Ф', "F"},
			{'Х', "KH"},
			{'Ц', "C"},
			{'Ч', "CH"},
			{'Ш', "SH"},
			{'Щ', "SHH"},
			{'Ъ', "III"},
			{'Ы', "Y"},
			{'Ь', "II"},
			{'Э', "EH"},
			{'Ю', "JU"},
			{'Я', "JA"}
		};
		private static readonly Dictionary<string, char> translitDecodeTbl = new Dictionary<string, char> 
		{
			{"a"  , 'а'},
			{"b"  , 'б'},
			{"v"  , 'в'},
			{"g"  , 'г'},
			{"d"  , 'д'},
			{"e"  , 'е'},
			{"eh" , 'э'},
			{"jo" , 'ё'},
			{"jj" , 'й'},
			{"ju" , 'ю'},
			{"ja" , 'я'},
			{"zh" , 'ж'},
			{"z"  , 'з'},
			{"i"  , 'и'},
			{"ii"  , 'ь'},
			{"iii" , 'ъ'},
			{"k"  , 'к'},
			{"kh" , 'х'},
			{"l"  , 'л'},
			{"m"  , 'м'},
			{"n"  , 'н'},
			{"o"  , 'о'},
			{"p"  , 'п'},
			{"r"  , 'р'},
			{"s"  , 'с'},
			{"sh" , 'ш'},
			{"shh", 'щ'},
			{"t"  , 'т'},
			{"u"  , 'у'},
			{"f"  , 'ф'},
			{"c"  , 'ц'},
			{"ch" , 'ч'},
			{"y"  , 'ы'},
			{"A"  , 'А'},
			{"B"  , 'Б'},
			{"V"  , 'В'},
			{"G"  , 'Г'},
			{"D"  , 'Д'},
			{"E"  , 'Е'},
			{"EH" , 'Э'},
			{"JO" , 'Ё'},
			{"JJ" , 'Й'},
			{"JU" , 'Ю'},
			{"JA" , 'Я'},
			{"ZH" , 'Ж'},
			{"Z"  , 'З'},
			{"I"  , 'И'},
			{"II"  , 'Ь'},
			{"III" , 'Ъ'},
			{"K"  , 'К'},
			{"KH" , 'Х'},
			{"L"  , 'Л'},
			{"M"  , 'М'},
			{"N"  , 'Н'},
			{"O"  , 'О'},
			{"P"  , 'П'},
			{"R"  , 'Р'},
			{"S"  , 'С'},
			{"SH" , 'Ш'},
			{"SHH", 'Щ'},
			{"T"  , 'Т'},
			{"U"  , 'У'},
			{"F"  , 'Ф'},
			{"C"  , 'Ц'},
			{"CH" , 'Ч'},
			{"Y"  , 'Ы'}
		};

		public static string TranslitEncode(this string item, bool escapeSpec = false)
		{
			if (item == null) return null;
			item = reSpaces.Replace(item, " ");
			var ret = new StringBuilder();
			var i = 0;
			while (i < item.Length)
			{
				switch (item[i])
				{
					case 'а':
					case 'б':
					case 'в':
					case 'г':
					case 'д':
					case 'е':
					case 'ё':
					case 'ж':
					case 'з':
					case 'и':
					case 'й':
					case 'к':
					case 'л':
					case 'м':
					case 'н':
					case 'о':
					case 'п':
					case 'р':
					case 'с':
					case 'т':
					case 'у':
					case 'ф':
					case 'х':
					case 'ц':
					case 'ч':
					case 'ш':
					case 'щ':
					case 'ъ':
					case 'ы':
					case 'ь':
					case 'э':
					case 'ю':
					case 'я':
					case 'А':
					case 'Б':
					case 'В':
					case 'Г':
					case 'Д':
					case 'Е':
					case 'Ё':
					case 'Ж':
					case 'З':
					case 'И':
					case 'Й':
					case 'К':
					case 'Л':
					case 'М':
					case 'Н':
					case 'О':
					case 'П':
					case 'Р':
					case 'С':
					case 'Т':
					case 'У':
					case 'Ф':
					case 'Х':
					case 'Ц':
					case 'Ч':
					case 'Ш':
					case 'Щ':
					case 'Ъ':
					case 'Ы':
					case 'Ь':
					case 'Э':
					case 'Ю':
					case 'Я':
						ret.Append(translitEncodeTbl[item[i]]);
						break;
					case 'a':
					case 'b':
					case 'c':
					case 'd':
					case 'e':
					case 'f':
					case 'g':
					case 'h':
					case 'i':
					case 'j':
					case 'k':
					case 'l':
					case 'm':
					case 'n':
					case 'o':
					case 'p':
					case 'q':
					case 'r':
					case 's':
					case 't':
					case 'u':
					case 'v':
					case 'w':
					case 'x':
					case 'y':
					case 'z':
					case 'A':
					case 'B':
					case 'C':
					case 'D':
					case 'E':
					case 'F':
					case 'G':
					case 'H':
					case 'I':
					case 'J':
					case 'K':
					case 'L':
					case 'M':
					case 'N':
					case 'O':
					case 'P':
					case 'Q':
					case 'R':
					case 'S':
					case 'T':
					case 'U':
					case 'V':
					case 'W':
					case 'X':
					case 'Y':
					case 'Z':
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
                    case '9':
                    case '_':
                        ret.Append(item[i]);
						break;
					case ' ':
						ret.Append("-");
						break;
					case '-':
						ret.Append("--");
						break;
					case '/':
					case '\\':
					case '.':
					case '&':
					case '?':
					case '+':
						if (escapeSpec) ret.Append("-");
						else ret.Append(item[i]);
						break;
					case '"':
					case '\'':
						break;
					default:
						if (escapeSpec) ret.Append("-");
						else ret.Append(item[i]);
						break;
				}
				i++;
			}
			return reMinus.Replace(ret.ToString(), "-");
		}

		public static string TranslitDecode(this string item)
		{
			var len = item.Length;
			var ret = new StringBuilder();

			for (int i = 0; i < item.Length; i++)
			{
				switch (item[i])
				{
					case 'a':
					case 'b':
					case 'v':
					case 'g':
					case 'd':
					case 'l':
					case 'm':
					case 'n':
					case 'o':
					case 'p':
					case 'r':
					case 't':
					case 'u':
					case 'f':
					case 'y':
					case 'A':
					case 'B':
					case 'V':
					case 'G':
					case 'D':
					case 'L':
					case 'M':
					case 'N':
					case 'O':
					case 'P':
					case 'R':
					case 'T':
					case 'U':
					case 'F':
					case 'Y':
						ret.Append(translitDecodeTbl[item[i].ToString()]);
						break;
					case '-':
						if (i + 1 < len && item[i + 1] == '-')
						{
							i++;
							ret.Append("-");
						}
						else ret.Append(" ");
						break;
					case 'e':
						if (i + 1 < len && item[i + 1] == 'h')
						{
							i++;
							ret.Append(translitDecodeTbl["eh"]);
						}
						else ret.Append(translitDecodeTbl["e"]);
						break;
					case 'E':
						if (i + 1 < len && item[i + 1] == 'H')
						{
							i++;
							ret.Append(translitDecodeTbl["EH"]);
						}
						else ret.Append(translitDecodeTbl["E"]);
						break;
					case 'i':
						if (i + 2 < len && item[i + 1] == 'i' && item[i + 2] == 'i')
						{
							i += 2;
							ret.Append(translitDecodeTbl["iii"]);
						}
						if (i + 1 < len && item[i + 1] == 'i')
						{
							i++;
							ret.Append(translitDecodeTbl["ii"]);
						}
						else ret.Append(translitDecodeTbl["i"]);
						break;
					case 'I':
						if (i + 2 < len && item[i + 1] == 'I' && item[i + 2] == 'I')
						{
							i += 2;
							ret.Append(translitDecodeTbl["III"]);
						}
						if (i + 1 < len && item[i + 1] == 'I')
						{
							i++;
							ret.Append(translitDecodeTbl["II"]);
						}
						else ret.Append(translitDecodeTbl["I"]);
						break;
					case 'j':
						if (i + 1 >= len) throw new Exception("Ошибка раскодирования строки, символ " + item[i] + " не может быть одиночным");
						i++;
						switch (item[i])
						{
							case 'o':
								ret.Append(translitDecodeTbl["jo"]);
								break;
							case 'j':
								ret.Append(translitDecodeTbl["jj"]);
								break;
							case 'u':
								ret.Append(translitDecodeTbl["ju"]);
								break;
							case 'a':
								ret.Append(translitDecodeTbl["ja"]);
								break;
							default:
								throw new Exception("Неправильная последовательность символов в строке, после символа j символ " + item[i]);
						}
						break;
					case 'J':
						if (i + 1 >= len) throw new Exception("Ошибка раскодирования строки, символ " + item[i] + " не может быть одиночным");
						i++;
						switch (item[i])
						{
							case 'O':
								ret.Append(translitDecodeTbl["JO"]);
								break;
							case 'J':
								ret.Append(translitDecodeTbl["JJ"]);
								break;
							case 'U':
								ret.Append(translitDecodeTbl["JU"]);
								break;
							case 'A':
								ret.Append(translitDecodeTbl["JA"]);
								break;
							default:
								throw new Exception("Неправильная последовательность символов в строке, после символа j символ " + item[i]);
						}
						break;
					case 'z':
						if (i + 1 < len && item[i + 1] == 'h')
						{
							i++;
							ret.Append(translitDecodeTbl["zh"]);
						}
						else ret.Append(translitDecodeTbl["z"]);
						break;
					case 'Z':
						if (i + 1 < len && item[i + 1] == 'H')
						{
							i++;
							ret.Append(translitDecodeTbl["ZH"]);
						}
						else ret.Append(translitDecodeTbl["Z"]);
						break;
					case 'k':
						if (i + 1 < len && item[i + 1] == 'h')
						{
							i++;
							ret.Append(translitDecodeTbl["kh"]);
						}
						else ret.Append(translitDecodeTbl["k"]);
						break;
					case 'K':
						if (i + 1 < len && item[i + 1] == 'H')
						{
							i++;
							ret.Append(translitDecodeTbl["KH"]);
						}
						else ret.Append(translitDecodeTbl["K"]);
						break;
					case 's':
						if (i + 1 < len && item[i + 1] == 'h')
						{
							i++;
							if (i + 1 < len && item[i + 1] == 'h')
							{
								ret.Append(translitDecodeTbl["shh"]);
								i++;
							}
							else ret.Append(translitDecodeTbl["sh"]);
						}
						else ret.Append(translitDecodeTbl["s"]);
						break;
					case 'S':
						if (i + 1 < len && item[i + 1] == 'H')
						{
							i++;
							if (i + 1 < len && item[i + 1] == 'H')
							{
								ret.Append(translitDecodeTbl["SHH"]);
								i++;
							}
							else ret.Append(translitDecodeTbl["SH"]);
						}
						else ret.Append(translitDecodeTbl["S"]);
						break;
					case 'c':
						if (i + 1 < len && item[i + 1] == 'h')
						{
							i++;
							ret.Append(translitDecodeTbl["ch"]);
						}
						else ret.Append(translitDecodeTbl["c"]);
						break;
					case 'C':
						if (i + 1 < len && item[i + 1] == 'H')
						{
							i++;
							ret.Append(translitDecodeTbl["CH"]);
						}
						else ret.Append(translitDecodeTbl["C"]);
						break;
					default:
						ret.Append(item[i]);
						break;
				}
			}

			return ret.ToString();
		}

        public static string TranslitAndNormalize(this string item)
        {
            return item.TranslitEncode(true).TrimEnd("-".ToCharArray()).TrimStart("-".ToCharArray());
        }
	}
}
