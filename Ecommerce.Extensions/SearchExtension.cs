﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Ecommerce.Extensions
{
	public static class SearchExtension
	{
		private static readonly Regex splitter = new Regex(@"\s+");
		public static string SplitToSearch(this string columnName, string query)
		{
			return splitter.Split(query)
				.ToList()
				.Aggregate(new StringBuilder(), 
				(sb, str) => {
					sb.AppendFormat("{0} like '%{1}%' and ", columnName, str);
				    return sb;
				},
				sb =>
					{
						if(sb.Length > 0) sb.Length -= 5;
						return string.Format("({0})", sb);
					});
			

		}
	}
}
