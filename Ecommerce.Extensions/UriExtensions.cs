﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Ecommerce.Extensions
{
	public static class UriExtensions
	{
		public static Uri RemoveParam(this Uri item, string paramName)
		{
			var query = item.Query;
			var par = HttpUtility.ParseQueryString(item.Query);
			par.Remove(paramName);
//			query = Regex.Replace(query, paramName+"=[^&]*", "", RegexOptions.IgnoreCase);
			UriBuilder builder = new UriBuilder(item);
			builder.Query = par.ToString();
			return builder.Uri;
		}
	}
}
