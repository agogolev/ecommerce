﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ecommerce.Extensions
{
	public static class ListGuidExtension
	{
		public static string InClause(this List<Guid> items)
		{
			var sb = new StringBuilder();
			sb.Append(" in (");
			return items.Select(g => string.Format("'{0}'", g)).Intersperse(",\r\n").Aggregate(sb, (b, s) => { b.Append(s); return b; })
				.Append(")").ToString();
		}
	}
}
