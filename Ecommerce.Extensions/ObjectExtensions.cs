﻿using System;

namespace Ecommerce.Extensions
{
	public static class ObjectExtensions
	{
		public static string ToSqlString(this object o)
		{
			if (o == null || o == DBNull.Value) return "null";
			var s = o as string;
			if (s != null)
				return "'" + s.Replace("'", "''") + "'";
			if ((o is Guid)) if (((Guid)o) != Guid.Empty) return ("'" + o + "'"); else return "null";
			if ((o is decimal)) return o.ToString().Replace(",", ".");
			if ((o is DateTime)) return "'" + ((DateTime)o).ToString("yyyyMMdd HH:mm:ss") + "'";
			if ((o is bool)) return (bool)o ? "1" : "0";
			return o.ToString();
		}
	}
}
