﻿using System.IO;

namespace Ecommerce.Extensions
{
	public static class CRC32Extension
	{
		public static uint CalculateCRC32(this Stream stream)
		{
			stream.Position = 0;
			try
			{
				const int bufferSize = 1024;
				const uint polynomial = 0xEDB88320;

				var result = 0xFFFFFFFF;

				var buffer = new byte[bufferSize];
				var table_CRC32 = new uint[256];

				unchecked
				{
					//
					// Инициалиазация таблицы
					//
					for (var i = 0; i < 256; i++)
					{
						var Crc32 = (uint)i;

						for (var j = 8; j > 0; j--)
						{
							if ((Crc32 & 1) == 1)
								Crc32 = (Crc32 >> 1) ^ polynomial;
							else
								Crc32 >>= 1;
						}

						table_CRC32[i] = Crc32;
					}

					//
					// Чтение из буфера
					//
					var count = stream.Read(buffer, 0, bufferSize);

					//
					// Вычисление CRC
					//
					while (count > 0)
					{
						for (int i = 0; i < count; i++)
						{
							result = ((result) >> 8)
								  ^ table_CRC32[(buffer[i])
								  ^ ((result) & 0x000000FF)];
						}

						count = stream.Read(buffer, 0, bufferSize);
					}
				}

				return ~result;
			}
			finally
			{
				stream.Position = 0;
			}

		}
	}
}
