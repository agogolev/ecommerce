﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Ecommerce.Extensions
{
	public static class TestExtension
	{
		public static string TraceProperties(this object item)
		{
			return item.GetType().GetProperties().ToList().Aggregate(new StringBuilder(), (builder, prop) =>
			{
				ParameterInfo[] indexed = prop.GetIndexParameters();
				if (indexed.Length == 0)
					builder.AppendFormat("{0}: {1}\r\n", prop.Name, prop.GetValue(item, null));
				else
				{
					builder.AppendFormat("{0} has {1} indexes\r\n", prop.Name, indexed.Length);
				}
				return builder;
			}, builder => builder.ToString());
		}
	}
}
