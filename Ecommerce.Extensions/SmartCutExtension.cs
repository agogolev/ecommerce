﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Bit.Extensions
{
	public static class SmartCutExtension
	{
		private static readonly Regex re = new Regex("\\s+", RegexOptions.RightToLeft);
		private static readonly Regex reTags = new Regex(@"\<[^>]+\>");
		public static string Cut(this string item, int maxLength)
		{
			if (item == null) return null;
			item = reTags.Replace(item, "");
			if (item.Length < maxLength) return item;
			var temp = item.Substring(0, maxLength - 3);
			var m = re.Match(temp);
			if (!m.Success)
				return string.Format("{0}...", temp);
			var i = m.Index;
			return string.Format("{0}...", temp.Substring(0, i));
		}
	}
}
