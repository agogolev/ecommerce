﻿using System.Text.RegularExpressions;

namespace Ecommerce.Extensions
{
	public static class StringExtension
	{
		//private static readonly Regex regex = new Regex("\\s+", RegexOptions.RightToLeft | RegexOptions.Compiled);
		private static readonly Regex regex = new Regex(@"\s{2,}");

		public static int IndexOfNoCase(this string item, string item1)
		{
			return item.ToLower().IndexOf(item1.ToLower(), System.StringComparison.Ordinal);
		}

		public static string FirstLetter(this string item)
		{
			if (item[0] >= '0' && item[0] <= '9') return "123";
			return item[0].ToString().ToUpper();
		}

		public static int? ParseId(this string item)
		{
			if (string.IsNullOrWhiteSpace(item)) return null;
			var id = item.LastIndexOf('_');
			int result;
			if (id != -1 && int.TryParse(item.Substring(id + 1), out result)) return result;
			return null;
		}

		public static string TrimInner(this string item)
		{
			return regex.Replace(item, " ");
		}

		public static string Cut(this string item, int maxLength, string prefix = " – ", string suffix = "...")
		{
			if (string.IsNullOrWhiteSpace(item)) return "";
			if (item.Length < maxLength) return string.Format("{0}{1}", prefix, item);
			var temp = item.Substring(0, maxLength - suffix.Length);
			var m = regex.Match(temp);
			if (!m.Success)
				return string.Format("{0}...", temp);
			var i = m.Index;
			return string.Format("{0}{1}{2}", prefix, temp.Substring(0, i), suffix);
		}

		public static string Decline(this string noun, int number, bool withoutNumber = false)
		{
			string suffix = string.Empty;

			if (number % 100 < 20)
			{
				switch (number % 100)
				{
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						suffix = "а";
						break;
					case 0:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
					case 13:
					case 14:
					case 15:
					case 16:
					case 17:
					case 18:
					case 19:
						suffix = "ов";
						break;
				}
			}
			else
			{
				switch ((number % 100) % 10)
				{
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						suffix = "а";
						break;
					case 0:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
						suffix = "ов";
						break;
				}
			}

			return !withoutNumber ? string.Format("{0} {1}{2}", number, noun, suffix) : string.Format("{0}{1}", noun, suffix);
		}

		public static bool IsModified(this string currentValue, string prevValue, string pattern)
		{
			var i = 0;
			pattern = Regex.Replace(pattern, @"%MODEL%", m => string.Format(@"(?<model{0}>.*)", i++));
			i = 0;
			pattern = Regex.Replace(pattern, @"%PRICE%", m => string.Format(@"(?<price{0}>.*)", i++));
			i = 0;
			pattern = Regex.Replace(pattern, @"%CATEGORY%", m => string.Format(@"(?<category{0}>.*)", i++));
			i = 0;
			pattern = Regex.Replace(pattern, @"%DELIVERY_PRICE%", m => string.Format(@"(?<deliveryPrice{0}>.*)", i++));
			return true;
		}
	}
}
