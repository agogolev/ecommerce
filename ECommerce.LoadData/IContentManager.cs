﻿using System.Threading.Tasks;
using System.Xml.Linq;
using MetaData;

namespace ECommerce.LoadData
{
    public interface IContentManager
    {
        void ConfigureData();
        void FillDataSet(XDocument doc);
    }
}