﻿using System;
using System.Xml.Linq;

namespace ECommerce.LoadData
{
    public interface IXmlParser
    {
        DateTime LoadDate { get; set; }
        void LoadXml(XDocument doc);
    }
}