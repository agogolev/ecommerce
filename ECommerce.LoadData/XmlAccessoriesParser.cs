﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using ECommerce.LoadData.Entities;
using MetaData;

namespace ECommerce.LoadData
{
    public class XmlAccessoriesParser : IXmlParser
    {
        public IList<AccessoryLink> Accessories { get; set; }
        public DateTime LoadDate { get; set; }

        public void LoadXml(XDocument doc)
        {
            var rootElement = doc.Root;
            LoadDate = DateTime.Parse(doc.Root?.Attribute("date")?.Value ?? "2001-01-01 12:00:00", CultureInfo.InvariantCulture.DateTimeFormat);

            Accessories = (rootElement?.Element("ExtraAccs")?.Elements("ExtraAcc").Select(e => new AccessoryLink
                {
                    Id = int.Parse(e.Attribute("id_postavshik")?.Value ?? "0"),
                    LinkId = int.Parse(e.Attribute("AccId")?.Value ?? "0"),
                    IsRequired = false
                }) ?? new List<AccessoryLink>())
                .Union(rootElement?.Element("ObligatoryAccs")?.Elements("ObligatoryAcc").Select(e => new AccessoryLink
                {
                    Id = int.Parse(e.Attribute("id_postavshik")?.Value ?? "0"),
                    LinkId = int.Parse(e.Attribute("AccId")?.Value ?? "0"),
                    IsRequired = true
                }) ?? new List<AccessoryLink>())
                .Distinct(new LambdaComparer<AccessoryLink>((a, b) => a.Id == b.Id && a.LinkId == b.LinkId)).ToList();
        }
    }
}
