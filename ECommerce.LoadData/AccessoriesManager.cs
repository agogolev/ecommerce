﻿using System.Data;
using System.Xml.Linq;

namespace ECommerce.LoadData
{
    public class AccessoriesManager : ContentManager
    {
        public override void ConfigureData()
        {
            _configureData.Tables.Clear();
            DataTable accessories = new DataTable("table");
            _configureData.Tables.Add(accessories);

            accessories.Columns.Add("id_postavshik", typeof(int));
            accessories.Columns.Add("id_accessory", typeof(int));
            accessories.Columns.Add("isRequired", typeof(bool));

        }

        public override void FillDataSet(XDocument doc)
        {
            var parser = new XmlAccessoriesParser();
            parser.LoadXml(doc);
            foreach (var accessoryLink in parser.Accessories)
            {
                _configureData.Tables["table"].Rows.Add(accessoryLink.Id, accessoryLink.LinkId, accessoryLink.IsRequired);
            }
        }
    }
}
