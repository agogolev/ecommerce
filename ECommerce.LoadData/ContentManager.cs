﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using MetaData;

namespace ECommerce.LoadData
{
    public abstract class ContentManager : IContentManager
    {
        protected DataSetISM _configureData = new DataSetISM();

        public abstract void ConfigureData();

        public abstract void FillDataSet(XDocument doc);

        public async Task<string> GetContent(string apiUrl)
        {
            ConfigureData();

            XDocument doc = await LoadContent(apiUrl);

            FillDataSet(doc);

            return _configureData.SaveToString();
        }

        private async Task<XDocument> LoadContent(string apiUrl)
        {
            var client = new HttpClient();
            HttpResponseMessage task = await client.GetAsync(new Uri(apiUrl));
            string xmlTask = await task.Content.ReadAsStringAsync();

            return XDocument.Parse(xmlTask);
        }
    }
}
