﻿using System;
using System.Data;
using System.Xml.Linq;

namespace ECommerce.LoadData
{
    public class GoodsManager : ContentManager
    {


        public override void FillDataSet(XDocument doc)
        {
            var parser = new XmlGoodsParser();
            parser.LoadXml(doc);
            foreach (var entity in parser.Manufacturers)
            {
                _configureData.Tables["table2"].Rows.Add(entity.Id, entity.Name);
            }

            foreach (var treeEntity in parser.Categories)
            {
                _configureData.Tables["table1"].Rows.Add(treeEntity.Id, treeEntity.ParentId, treeEntity.Name);
            }

            foreach (var goods in parser.GoodsItems)
            {
                _configureData.Tables["table"].Rows.Add(
                    goods.Id,
                    goods.Name,
                    goods.Type,
                    goods.SalesNotes,
                    goods.ManufacturerId,
                    goods.ClassId4,
                    goods.PseudoID,
                    goods.DeliveryOptionsDays,
                    goods.DeliveryOptionsBefore,
                    goods.Promo,
                    goods.Price,
                    goods.PriceForYM,
                    goods.SalePrice.HasValue ? (object)goods.SalePrice.Value : DBNull.Value,
                    goods.PriceD,
                    goods.PriceD2,
                    goods.PriceDS,
                    goods.PriceDostRise,
                    goods.Ean,
                    goods.Cpa,
                    goods.Pm,
                    goods.TovarAction,
                    (object)goods.TovarActionDate ?? DBNull.Value,
                    goods.Popularity,
                    goods.Acquiring,
                    goods.Weight,
                    goods.Dimensions,
                    goods.SecondYML
                );
            }

            foreach (var promoCode in parser.PromoCodes)
            {
                _configureData.Tables["table3"].Rows.Add(
                    promoCode.PromoCodeID,
                    promoCode.Description,
                    promoCode.Sum,
                    promoCode.Url
                );
                foreach (var item in promoCode.Items)
                {
                    _configureData.Tables["table4"].Rows.Add(
                        promoCode.PromoCodeID,
                        item.GoodsID,
                        item.YaLoad
                    );
                }
            }
        }

        public override void ConfigureData()
        {
            _configureData.Tables.Clear();
            DataTable goods = new DataTable("table");
            _configureData.Tables.Add(goods);
            DataTable catalog = new DataTable("table1");
            _configureData.Tables.Add(catalog);
            DataTable manuf = new DataTable("table2");
            _configureData.Tables.Add(manuf);
            DataTable promoCodes = new DataTable("table3");
            _configureData.Tables.Add(promoCodes);
            DataTable promoCodeGoods = new DataTable("table4");
            _configureData.Tables.Add(promoCodeGoods);

            goods.Columns.Add("id_postavshik", typeof(int));
            goods.Columns.Add("model");
            goods.Columns.Add("productType");
            goods.Columns.Add("sales_notes");
            goods.Columns.Add("manufacturerId", typeof(int));
            goods.Columns.Add("classId", typeof(int));
            goods.Columns.Add("pseudoID");
            goods.Columns.Add("delivery_options_days");
            goods.Columns.Add("delivery_options_before", typeof(int));
            goods.Columns.Add("promo", typeof(int));
            goods.Columns.Add("price", typeof(int));
            goods.Columns.Add("priceForYM", typeof(int));
            goods.Columns.Add("salePrice", typeof(int));
            goods.Columns.Add("priceD", typeof(int));
            goods.Columns.Add("priceD2", typeof(int));
            goods.Columns.Add("priceDS", typeof(int));
            goods.Columns.Add("priceDostRise", typeof(int));
            goods.Columns.Add("EAN");
            goods.Columns.Add("CPA", typeof(int));
            goods.Columns.Add("PM", typeof(int));
            goods.Columns.Add("TovarAction", typeof(int));
            goods.Columns.Add("TovarActionDate", typeof(DateTime));
            goods.Columns.Add("popularity", typeof(int));
            goods.Columns.Add("acquiring", typeof(bool));
            goods.Columns.Add("weight");
            goods.Columns.Add("dimensions");
            goods.Columns.Add("secondYML", typeof(bool));



            catalog.Columns.Add("id", typeof(int));
            catalog.Columns.Add("parentId", typeof(int));
            catalog.Columns.Add("name");

            manuf.Columns.Add("id", typeof(int));
            manuf.Columns.Add("name");

            promoCodes.Columns.Add("promoName");
            promoCodes.Columns.Add("description");
            promoCodes.Columns.Add("sum", typeof(decimal));
            promoCodes.Columns.Add("url");

            promoCodeGoods.Columns.Add("promoName");
            promoCodeGoods.Columns.Add("id_postavshik", typeof(int));
            promoCodeGoods.Columns.Add("ym", typeof(bool));
        }
    }
}