using System;

namespace ECommerce.LoadData.Entities
{
    public class Goods : Entity
    {
        public string Type { get; set; }
        public string SalesNotes { get; set; }
        public string DeliveryOptionsDays { get; set; }
        public int DeliveryOptionsBefore { get; set; }
        public string Ean { get; set; }
        public int ManufacturerId { get; set; }
        public int Promo { get; set; }
        public int TovarAction { get; set; }
        public int PseudoID { get; set; }
        public int ClassId1 { get; set; }
        public int ClassId2 { get; set; }
        public int ClassId3 { get; set; }
        public int ClassId4 { get; set; }
        public int Pm { get; set; }
        public int Cpa { get; set; }
        public int Price { get; set; }
        public int PriceForYM { get; set; }
        public int? SalePrice { get; set; }
        public int PriceD { get; set; }
        public int PriceD2 { get; set; }
        public int PriceDS { get; set; }
        public int PriceDostRise { get; set; }
        public DateTime? TovarActionDate { get; set; }
        public int Popularity { get; set; }
        public bool Acquiring { get; set; }
        public string Weight { get; set; }
        public string Dimensions { get; set; }
        public bool SecondYML { get; set; }
    }
}