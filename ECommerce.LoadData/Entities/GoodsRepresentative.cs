﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.LoadData.Entities
{
    internal class GoodsRepresentative : Goods
    {
        public string InnerXml { get; set; }
    }
}
