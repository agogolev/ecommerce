﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.LoadData.Entities
{
    public class PromoCode : Entity
    {
        public string PromoCodeID { get; set; }
        public decimal Sum { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public IList<PromoCodeGoods> Items { get; set; }
    }
}
