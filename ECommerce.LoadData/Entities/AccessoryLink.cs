﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECommerce.LoadData.Entities
{
    public class AccessoryLink
    {
        public int Id { get; set; }
        public int LinkId { get; set; }
        public bool IsRequired { get; set; }
    }
}
