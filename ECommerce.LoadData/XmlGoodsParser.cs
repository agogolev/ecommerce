﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using ECommerce.LoadData.Entities;
using NLog;

namespace ECommerce.LoadData
{
    public class XmlGoodsParser : IXmlParser
    {
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        public DateTime LoadDate { get; set; }

        public IList<Entity> Manufacturers { get; set; }
        public IList<TreeEntity> Categories { get; set; }
        public IList<Goods> GoodsItems { get; set; }
        public IList<PromoCode> PromoCodes { get; set; }

        public void LoadXml(XDocument doc)
        {
            var rootElement = doc.Root;
            LoadDate = DateTime.Parse(doc.Root?.Attribute("date")?.Value ?? "2001-01-01 12:00:00", CultureInfo.InvariantCulture.DateTimeFormat);

            Manufacturers = rootElement?.Element("manufacturers")?.Elements("manufacturer").Select(e => new Entity
            {
                Id = int.Parse(e.Attribute("manufactureId")?.Value ?? "0"),
                Name = e.Attribute("manufacturerName")?.Value ?? ""
            }).ToList() ?? new List<Entity>();

            Categories = rootElement?.Element("classes")?.Elements("class").Select(e => new TreeEntity
            {
                Id = int.Parse(e.Attribute("classId")?.Value ?? "0"),
                Name = e.Attribute("className")?.Value ?? ""
            }).ToList() ?? new List<TreeEntity>();

            var goodsQuery = rootElement?.Element("products")?.Elements("product").Select(e => new Goods
            {
                Id = int.Parse(e.Element("id_postavshik")?.Value ?? "0"),
                Name = e.Element("model")?.Value ?? "",
                Type = e.Element("productType")?.Value ?? "",
                SalesNotes = e.Element("sales_notes")?.Value ?? "",
                DeliveryOptionsDays = e.Element("delivery_options_days")?.Value ?? "",
                DeliveryOptionsBefore = int.Parse(e.Element("delivery_options_before")?.Value ?? "18"),
                Ean = e.Element("EAN")?.Value ?? "",
                ManufacturerId = int.Parse(e.Element("manufactureId")?.Value ?? "0"),
                Promo = 0, //int.Parse(e.Element("promo")?.Value ?? "0"),
                TovarAction = int.Parse(e.Element("TovarAction")?.Value ?? "0"),
                PseudoID = int.Parse(e.Element("PseudoID")?.Value ?? "0"),
                ClassId1 = int.Parse(e.Element("classId1")?.Value ?? "0"),
                ClassId2 = int.Parse(e.Element("classId2")?.Value ?? "0"),
                ClassId3 = int.Parse(e.Element("classId3")?.Value ?? "0"),
                ClassId4 = int.Parse(e.Element("classId4")?.Value ?? "0"),
                Pm = int.Parse(e.Element("PM")?.Value ?? "0"),
                Cpa = int.Parse(e.Element("CPA")?.Value ?? "0"),
                Price = int.Parse(e.Element("price")?.Value ?? "0"),
                PriceForYM = int.Parse(e.Element("priceForYM")?.Value ?? "0"),
                SalePrice = (int?)e.Element("SalePrice"),
                PriceD = int.Parse(e.Element("priceD")?.Value ?? "0"),
                PriceD2 = int.Parse(e.Element("priceD2")?.Value ?? "0"),
                PriceDS = int.Parse(e.Element("priceDS")?.Value ?? "0"),
                PriceDostRise = int.Parse(e.Element("priceDostRise")?.Value ?? "0"),
                TovarActionDate = e.Element("TovarActionDate") != null && e.Element("TovarActionDate")?.Value != "NULL" ? DateTime.Parse(e.Element("TovarActionDate")?.Value) : (DateTime?)null,
                Popularity = int.Parse(e.Element("popularity")?.Value ?? "0"),
                Acquiring = int.Parse(string.IsNullOrEmpty(e.Element("acquiring")?.Value) ? "0" : e.Element("acquiring")?.Value ?? "0") != 0,
                Weight = e.Element("weight")?.Value,
                Dimensions = e.Element("dimensions")?.Value,
                SecondYML = (e.Element("secondYML")?.Value ?? "0") == "1"

            });
            GoodsItems = new List<Goods>();

            if (goodsQuery != null)
            {
                int i = 0;
                try
                {
                    foreach (Goods goods in goodsQuery)
                    {
                        i++;
                        GoodsItems.Add(goods);
                    }
                }
                catch (Exception e)
                {
                    _log.Error($"Error of goods list creation. Previously created goods N = {i}, ID = {GoodsItems.Last().Id}");
                    throw;
                }
            }

            //.ToList() ?? new List<Goods>();

            Dictionary<int, int> processed = new Dictionary<int, int>();
            foreach (Goods goods in GoodsItems)
            {
                if (processed.ContainsKey(goods.ClassId4)) continue;

                var currentCategory = Categories?.Single(c => c.Id == goods.ClassId4);
                if (currentCategory != null) currentCategory.ParentId = goods.ClassId3;
                currentCategory = Categories?.Single(c => c.Id == goods.ClassId3);
                if (currentCategory != null) currentCategory.ParentId = goods.ClassId2;
                currentCategory = Categories?.Single(c => c.Id == goods.ClassId2);
                if (currentCategory != null) currentCategory.ParentId = goods.ClassId1;
                processed[goods.ClassId1] = 1;
                processed[goods.ClassId2] = 1;
                processed[goods.ClassId3] = 1;
                processed[goods.ClassId4] = 1;
            }

            for (int i = Categories.Count - 1; i >= 0; i--)
            {
                if (!processed.ContainsKey(Categories[i].Id)) Categories.RemoveAt(i);
            }

            PromoCodes = rootElement?.Element("promocodes")?.Elements("promocode").Select(e => new PromoCode
            {
                PromoCodeID = e.Element("PromoName")?.Value,
                Description = e.Element("description")?.Value,
                Url = e.Element("url")?.Value,
                Sum = decimal.Parse(e.Element("sum")?.Value ?? "0"),
                Items = e.Element("products")?.Elements("product").Select(e1 => new PromoCodeGoods
                {
                    GoodsID = int.Parse(e1.Element("id_postavshik")?.Value ?? "0"),
                    YaLoad = e1.Element("YM")?.Value == "1"

                }).ToList() ?? new List<PromoCodeGoods>()
            }
            ).ToList() ?? new List<PromoCode>();
        }
    }
}